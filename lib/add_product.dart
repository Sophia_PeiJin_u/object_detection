import 'dart:io';

import 'package:currency_picker/currency_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tags/flutter_tags.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:object_detection/controller/productController.dart';

class AddNewProduct extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        leading: TextButton(
          child: Icon(
            Icons.chevron_left,
            size: 20.0,
            color: Colors.black,
          ),
          style: TextButton.styleFrom(
            backgroundColor: Colors.transparent,
            primary: Colors.black,
            minimumSize: Size(30, 30),
            shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
          ),
          onPressed: () {
            Get.back();
          },
        ),
        backgroundColor: Colors.white,
        title: Text('Add new product'.tr,
            style: GoogleFonts.inter(
              color: Colors.black,
              fontSize: 17,
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.w400,
            )),
        centerTitle: true,
        actions: <Widget>[
          TextButton(
            child: Icon(
              Icons.settings_outlined,
              size: 20.0,
              color: Colors.black,
            ),
            style: TextButton.styleFrom(
              backgroundColor: Colors.transparent,
              primary: Colors.black,
              minimumSize: Size(30, 30),
              shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
            ),
            onPressed: () {
              Get.back();
              //Get.put(DrawerDisplayController()).updatedrawerType(1);
              //Scaffold.of(context).openEndDrawer();
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            //photo
            Center(
              child: Column(
                children: [
                  Container(
                    width: 320,
                    height: 320,
                    child: Card(
                      color: Color(0xffF5F5F5),
                      child: ProductController.to.product.value.photos.isEmpty
                          ? Center(
                              child: Text(
                              'Please go to the camera view and add more product photos',
                              style: TextStyle(
                                fontSize: 30,
                                color: Colors.grey,
                              ),
                              textAlign: TextAlign.center,
                            ))
                          : Image.file(
                              File(ProductController
                                  .to.product.value.photos.last),
                              fit: BoxFit.cover,
                            ),
                    ),
                  ),
                  OutlinedButton.icon(
                    onPressed: () {},
                    icon: Icon(
                      Icons.add,
                      color: Colors.black,
                    ),
                    label: Text(
                      'Add more photos',
                      style: TextStyle(fontSize: 12, color: Colors.black),
                    ),
                    style: ElevatedButton.styleFrom(
                      side: BorderSide(color: Colors.black),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            //product name
            Container(
              padding: EdgeInsets.fromLTRB(18.0, 0.0, 18.0, 10.0),
              child: Column(
                children: [
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      'Product name',
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                  TextField(
                    autofocus: false,
                    style: GoogleFonts.inter(
                      color: Colors.black,
                      fontSize: 14,
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.w400,
                    ),
                    decoration: InputDecoration(
                      fillColor: Color(0xffF5F5F5),
                      focusColor: Color(0xffF5F5F5),
                      hoverColor: Color(0xffF5F5F5),
                      filled: true,
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent),
                        borderRadius: BorderRadius.circular(30),
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent),
                        borderRadius: BorderRadius.circular(30),
                      ),
                      suffixIcon: TextButton(
                        child: Icon(
                          Icons.mic,
                          size: 20.0,
                          color: Colors.black,
                        ),
                        style: TextButton.styleFrom(
                          backgroundColor: Colors.transparent,
                          primary: Colors.black,
                          minimumSize: Size(30, 30),
                          shape: CircleBorder(
                              side: BorderSide(color: Colors.transparent)),
                        ),
                        onPressed: () {
                          print('speach to text runnung');
                        },
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(30),
                        ),
                      ),
                      hintText: 'enter product name'.tr,
                    ),
                  ),
                ],
              ),
            ),
            //selection
            Container(
              padding: EdgeInsets.fromLTRB(18.0, 0.0, 18.0, 10.0),
              child: Column(
                children: [
                  Align(
                    alignment: Alignment.topLeft,
                    child: Column(
                      children: [
                        Text(
                          'Selection',
                          style: TextStyle(fontSize: 18),
                        ),
                      ],
                    ),
                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Column(
                      children: [
                        ProductController.to.product.value.photos.isEmpty
                            ? SizedBox(height: 15,)
                            : Tags(
                          horizontalScroll: true,
                          itemCount: ProductController.to.product.value.results.last.length, // required
                          itemBuilder: (int index) {
                            final item =
                            ProductController.to.product.value.results.last[index];
                            return ItemTags(
                              // Each ItemTags must contain a Key. Keys allow Flutter to
                              // uniquely identify widgets.
                              //padding: EdgeInsets.only(top: 20),
                              key: Key(index.toString()),
                              index: index, // required
                              title: item.label,
                              textStyle: GoogleFonts.inter(
                                color: Colors.white,
                                fontSize: 12,
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.w400,
                              ),
                              textColor: Colors.white,
                              borderRadius: BorderRadius.circular(60),
                              elevation: 0,
                              combine: ItemTagsCombine.onlyText,
                              color: Color(0xff1874E5),
                              activeColor: Color(0xff4F4F4F),
                              // onPressed: (item) => print(item),
                              // onLongPressed: (item) => print(item),
                            );
                          },
                        ),
                      ],
                    ),
                  ),

                ],
              ),
            ),
            //description
            Container(
              padding: EdgeInsets.fromLTRB(18.0, 0.0, 18.0, 10.0),
              child: Column(
                children: [
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      'Description',
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                  TextField(
                    autofocus: false,
                    style: GoogleFonts.inter(
                      color: Colors.black,
                      fontSize: 14,
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.w400,
                    ),
                    decoration: InputDecoration(
                      fillColor: Color(0xffF5F5F5),
                      focusColor: Color(0xffF5F5F5),
                      hoverColor: Color(0xffF5F5F5),
                      filled: true,
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      contentPadding: const EdgeInsets.symmetric(vertical: 50.0),
                      suffixIcon: TextButton(
                        child: Icon(
                          Icons.mic,
                          size: 20.0,
                          color: Colors.black,
                        ),
                        style: TextButton.styleFrom(
                          backgroundColor: Colors.transparent,
                          primary: Colors.black,
                          minimumSize: Size(30, 30),
                          shape: CircleBorder(
                              side: BorderSide(color: Colors.transparent)),
                        ),
                        onPressed: () {
                          print('speach to text runnung');
                        },
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(10),
                        ),
                      ),
                      hintText: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'.tr,
                    ),
                  ),
                ],
              ),
            ),
            //tags-public/home/friend
            //price: currency doesn't refresh
            Container(
            padding: EdgeInsets.fromLTRB(18.0, 0.0, 18.0, 10.0),
              child: Column(
                children: [
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      'Price',
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                  Row(
                    children: [
                      Expanded(
                        flex: 2,
                        child: TextField(
                          autofocus: false,
                          style: GoogleFonts.inter(
                            color: Colors.black,
                            fontSize: 14,
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.w400,
                          ),
                          decoration: InputDecoration(
                            fillColor: Color(0xffF5F5F5),
                            focusColor: Color(0xffF5F5F5),
                            hoverColor: Color(0xffF5F5F5),
                            filled: true,
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.transparent),
                              borderRadius: BorderRadius.circular(30),
                            ),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.transparent),
                              borderRadius: BorderRadius.circular(30),
                            ),
                            suffixIcon: TextButton(
                              child: Icon(
                                Icons.mic,
                                size: 20.0,
                                color: Colors.black,
                              ),
                              style: TextButton.styleFrom(
                                backgroundColor: Colors.transparent,
                                primary: Colors.black,
                                minimumSize: Size(30, 30),
                                shape: CircleBorder(
                                    side: BorderSide(color: Colors.transparent)),
                              ),
                              onPressed: () {
                                print('speach to text runnung');
                              },
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(30),
                              ),
                            ),
                            hintText: '0.00'.tr,
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: TextButton(
                        child: Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Row(
                              mainAxisAlignment:
                              MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text('AUD',
                                    style: GoogleFonts.inter(
                                      color: Colors.black,
                                      fontSize: 17,
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.w400,
                                    )),
                                Icon(Icons.expand_more)
                              ]),
                        ),
                        style: TextButton.styleFrom(
                          backgroundColor: Color(0xffF5F5F5),
                          primary: Colors.black,
                          minimumSize: Size(230, 40),
                          shape: RoundedRectangleBorder(
                              side: BorderSide(
                                  color: Color(0xffF5F5F5),
                                  width: 1,
                                  style: BorderStyle.solid),
                              borderRadius:
                              BorderRadius.all(Radius.circular(30))),
                        ),
                        onPressed: () {
                          showCurrencyPicker(
                            context: context,
                            showFlag: true,
                            showCurrencyName: true,
                            showCurrencyCode: true,
                            onSelect: (Currency currency) {
                              print('Select currency: ${currency.name}');
                            },
                          );
                        },
                      ),)

                    ],
                  ),
                ],
              ),
            )
            //publish to

          ],
        ),
      ),
    );
  }
}
