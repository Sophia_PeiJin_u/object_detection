import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:object_detection/controller/drawerDisplayController.dart';

class SnapplaAppBar extends StatelessWidget with PreferredSizeWidget {
  @override
  final Size preferredSize;

  SnapplaAppBar({
    Key key,
  })  : preferredSize = Size.fromHeight(50.0),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return PreferredSize(
        preferredSize: Size.fromHeight(50.0), // here the desired height
        child: new AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          actions: <Widget>[
            FlatButton(
              //padding: EdgeInsets.only(right: 5.0),
              textColor: Colors.transparent,
              onPressed: () {
                Get.put(DrawerDisplayController()).updatedrawerType(1);
                Scaffold.of(context).openEndDrawer();
              },
              child: Icon(
                Icons.settings_outlined,
                size: 20.0,
                color: Colors.white,
              ),
              shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
            ),
          ],
        ));
  }
}
