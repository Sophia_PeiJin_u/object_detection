import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:get/get.dart';
import 'package:currency_picker/currency_picker.dart';
import 'package:object_detection/controller/drawerDisplayController.dart';

enum DetailsSelection { profileSettings, turnOnAllDetails, turnOffAllDetails }

class SnapplaDrawerNewProduct extends StatelessWidget {
  const SnapplaDrawerNewProduct({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    DetailsSelection _detailSelection = DetailsSelection.profileSettings;
    return Drawer(
        child: Container(
            color: Color(0xFF5F5F5),
            child: ListView(
                shrinkWrap: true,
                physics: ClampingScrollPhysics(),
                padding: const EdgeInsets.all(20.0),
                children: <Widget>[
                  ListTile(
                    leading: Icon(Icons.chevron_left_outlined),
                    title: Text('new_product'.tr,
                        style: GoogleFonts.inter(
                          color: Colors.black,
                          fontSize: 17,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w400,
                        )),
                    onTap: () {
                      Get.put(DrawerDisplayController()).updatedrawerType(1);
                    },
                  ),
                  Padding(
                      padding: EdgeInsets.only(left: 16),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Text('details_selection'.tr,
                                style: GoogleFonts.inter(
                                  color: Colors.black,
                                  fontSize: 12,
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w400,
                                ))
                          ])),
                  Padding(
                      padding: EdgeInsets.only(left: 16),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                                child: Text('details_selection_dsc'.tr,
                                    overflow: TextOverflow.ellipsis,
                                    //textDirection: TextDirection.rtl,
                                    textAlign: TextAlign.left,
                                    maxLines: 3,
                                    style: GoogleFonts.inter(
                                      color: Colors.grey,
                                      fontSize: 12,
                                      fontStyle: FontStyle.italic,
                                      fontWeight: FontWeight.w400,
                                    )))
                          ])),
                  Column(
                    children: <Widget>[
                      RadioListTile<DetailsSelection>(
                        title: Text('based_on_profile_settings'.tr,
                            style: GoogleFonts.inter(
                              color: Colors.black,
                              fontSize: 13,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w400,
                            )),
                        value: DetailsSelection.profileSettings,
                        groupValue: _detailSelection,
                        onChanged: (DetailsSelection value) {
                          _detailSelection = value;
                        },
                      ),
                      RadioListTile<DetailsSelection>(
                        title: Text('turn_on_all_details'.tr,
                            style: GoogleFonts.inter(
                              color: Colors.black,
                              fontSize: 13,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w400,
                            )),
                        value: DetailsSelection.turnOnAllDetails,
                        groupValue: _detailSelection,
                        onChanged: (DetailsSelection value) {
                          _detailSelection = value;
                        },
                      ),
                      RadioListTile<DetailsSelection>(
                        title: Text('turn_off_all_details'.tr,
                            style: GoogleFonts.inter(
                              color: Colors.black,
                              fontSize: 13,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w400,
                            )),
                        value: DetailsSelection.turnOffAllDetails,
                        groupValue: _detailSelection,
                        onChanged: (DetailsSelection value) {
                          _detailSelection = value;
                        },
                      ),
                    ],
                  ),
                  ListTile(
                    isThreeLine: true,
                    title: Text('remember_last_settings'.tr,
                        style: GoogleFonts.inter(
                          color: Colors.black,
                          fontSize: 13,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w400,
                        )),
                    onTap: () {},
                    subtitle: Column(children: <Widget>[
                      Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                                child: Text('remembered_dsc'.tr,
                                    overflow: TextOverflow.ellipsis,
                                    textAlign: TextAlign.left,
                                    maxLines: 3,
                                    style: GoogleFonts.inter(
                                      color: Colors.grey,
                                      fontSize: 12,
                                      fontStyle: FontStyle.italic,
                                      fontWeight: FontWeight.w400,
                                    )))
                          ]),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            TextButton(
                              child: Text('save'.tr,
                                  style: GoogleFonts.inter(
                                    color: Colors.white,
                                    fontSize: 17,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.w400,
                                  )),
                              style: TextButton.styleFrom(
                                backgroundColor: Color(0xff3ACC3B),
                                primary: Colors.black,
                                minimumSize: Size(101, 33),
                                shape: RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(20))),
                              ),
                              onPressed: () {
                                print('Pressed Save');
                              },
                            )
                          ])
                    ]),
                  ),
                  ListTile(
                    isThreeLine: true,
                    title: Text('default_currency'.tr,
                        style: GoogleFonts.inter(
                          color: Colors.black,
                          fontSize: 13,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w400,
                        )),
                    onTap: () {},
                    subtitle: Column(children: <Widget>[
                      Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                                child: Text(''.tr,
                                    overflow: TextOverflow.ellipsis,
                                    textAlign: TextAlign.left,
                                    maxLines: 3,
                                    style: GoogleFonts.inter(
                                      color: Colors.grey,
                                      fontSize: 12,
                                      fontStyle: FontStyle.italic,
                                      fontWeight: FontWeight.w400,
                                    )))
                          ]),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            TextButton(
                              child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text('AUD',
                                        style: GoogleFonts.inter(
                                          color: Colors.black,
                                          fontSize: 17,
                                          fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.w400,
                                        )),
                                    Icon(Icons.expand_more)
                                  ]),
                              style: TextButton.styleFrom(
                                backgroundColor: Colors.white,
                                primary: Colors.black,
                                minimumSize: Size(230, 40),
                                shape: RoundedRectangleBorder(
                                    side: BorderSide(
                                        color: Colors.grey,
                                        width: 1,
                                        style: BorderStyle.solid),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(20))),
                              ),
                              onPressed: () {
                                showCurrencyPicker(
                                  context: context,
                                  showFlag: true,
                                  showCurrencyName: true,
                                  showCurrencyCode: true,
                                  onSelect: (Currency currency) {
                                    print('Select currency: ${currency.name}');
                                  },
                                );
                              },
                            )
                          ])
                    ]),
                  )
                ])));
  }
}
