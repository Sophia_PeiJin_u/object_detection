// import 'package:flutter/material.dart';
// import 'package:google_fonts/google_fonts.dart';
// import 'package:get/get.dart';
//
// class SnapplaDrawerYourAccount extends StatelessWidget {
//   SnapplaDrawerYourAccount({Key key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     var radius = Radius.circular(20);
//     return Obx(() => DefaultTabController(
//         length: Get.put(ProfileController()).tabProfiles.length,
//         initialIndex: 0,
//         child: Drawer(
//             child: Container(
//           color: Color(0xFF5F5F5),
//           child: ListView(
//             shrinkWrap: true,
//             physics: ClampingScrollPhysics(),
//             padding: const EdgeInsets.all(20.0),
//             children: <Widget>[
//               ListTile(
//                 leading: Icon(Icons.chevron_left_outlined),
//                 title: Text('your_account'.tr,
//                     style: GoogleFonts.inter(
//                       color: Colors.black,
//                       fontSize: 17,
//                       fontStyle: FontStyle.normal,
//                       fontWeight: FontWeight.w400,
//                     )),
//                 onTap: () {
//                   Get.put(DrawerDisplayController()).updatedrawerType(1);
//                 },
//               ),
//               Row(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: <Widget>[
//                     Icon(
//                       Icons.person_pin,
//                       color: Colors.blue,
//                       size: 60.0,
//                     )
//                   ]),
//               Row(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: <Widget>[
//                     Text('your_name'.tr,
//                         style: GoogleFonts.inter(
//                           color: Colors.blue,
//                           fontSize: 17,
//                           fontStyle: FontStyle.normal,
//                           fontWeight: FontWeight.w400,
//                         ))
//                   ]),
//               Row(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: <Widget>[
//                     Icon(
//                       Icons.group,
//                       color: Colors.grey,
//                       size: 16.0,
//                     ),
//                     Text('Group name',
//                         style: GoogleFonts.inter(
//                           color: Colors.grey,
//                           fontSize: 12,
//                           fontStyle: FontStyle.normal,
//                           fontWeight: FontWeight.w400,
//                         ))
//                   ]),
//               SizedBox(
//                 height: 24.0,
//               ),
//               Row(
//                   mainAxisAlignment: MainAxisAlignment.start,
//                   children: <Widget>[
//                     Text("${'account_type'.tr}:",
//                         style: GoogleFonts.inter(
//                           color: Colors.black,
//                           fontSize: 12,
//                           fontStyle: FontStyle.normal,
//                           fontWeight: FontWeight.w400,
//                         ))
//                   ]),
//               Row(
//                   mainAxisAlignment: MainAxisAlignment.start,
//                   children: <Widget>[
//                     Expanded(
//                         child: Text('account_type_desc'.tr,
//                             overflow: TextOverflow.ellipsis,
//                             textDirection: TextDirection.rtl,
//                             textAlign: TextAlign.left,
//                             maxLines: 3,
//                             style: GoogleFonts.inter(
//                               color: Colors.grey,
//                               fontSize: 12,
//                               fontStyle: FontStyle.italic,
//                               fontWeight: FontWeight.w400,
//                             )))
//                   ]),
//               SizedBox(
//                 height: 24.0,
//               ),
//               Material(
//                   color: Colors.transparent,
//                   child: TabBar(
//                     onTap: (int index) {
//                       Get.put(ProfileController()).updatetabindex(index);
//                     },
//                     indicator: ShapeDecoration(
//                         shape: RoundedRectangleBorder(
//                             borderRadius: BorderRadius.only(
//                                 topRight: radius, topLeft: radius)),
//                         color: Colors.blue),
//                     isScrollable: true,
//                     unselectedLabelStyle: GoogleFonts.inter(
//                       color: Colors.blue,
//                       fontSize: 12,
//                       fontStyle: FontStyle.normal,
//                       fontWeight: FontWeight.w400,
//                     ),
//                     unselectedLabelColor: Colors.blue,
//                     labelStyle: GoogleFonts.inter(
//                       color: Colors.blue,
//                       fontSize: 12,
//                       fontStyle: FontStyle.normal,
//                       fontWeight: FontWeight.w400,
//                     ),
//                     tabs: Get.put(ProfileController()).tabProfiles,
//                   )),
//               IndexedStack(
//                 children: Get.put(ProfileController())
//                     .profileList
//                     .map((Profile profile) {
//                   return SnapplaDrawerYourAccountPreferences(profile: profile);
//                 }).toList(),
//                 index: Get.put(ProfileController()).tabindex.value,
//               ),
//             ],
//           ),
//         ))));
//   }
// }
