import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:get/get.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:object_detection/controller/drawerDisplayController.dart';

enum CleanType { currentSession, oneDay, oneWeek, never }

class SnapplaDrawerTrash extends StatelessWidget {
  const SnapplaDrawerTrash({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    CleanType _cleanType = CleanType.currentSession;
    return Drawer(
        child: Container(
            color: Color(0xFF5F5F5),
            child: ListView(
                shrinkWrap: true,
                physics: ClampingScrollPhysics(),
                padding: const EdgeInsets.all(20.0),
                children: <Widget>[
                  ListTile(
                    leading: Icon(Icons.chevron_left_outlined),
                    title: Text('snappla_trash'.tr,
                        style: GoogleFonts.inter(
                          color: Colors.black,
                          fontSize: 17,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w400,
                        )),
                    onTap: () {
                      Get.put(DrawerDisplayController()).updatedrawerType(1);
                    },
                  ),
                  Padding(
                      padding: EdgeInsets.only(left: 16),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Text('automatic_cleaning'.tr,
                                style: GoogleFonts.inter(
                                  color: Colors.black,
                                  fontSize: 12,
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w400,
                                ))
                          ])),
                  Padding(
                      padding: EdgeInsets.only(left: 16),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                                child: Text('automatic_cleaning_dsc'.tr,
                                    overflow: TextOverflow.ellipsis,
                                    //textDirection: TextDirection.rtl,
                                    textAlign: TextAlign.left,
                                    maxLines: 3,
                                    style: GoogleFonts.inter(
                                      color: Colors.grey,
                                      fontSize: 12,
                                      fontStyle: FontStyle.italic,
                                      fontWeight: FontWeight.w400,
                                    )))
                          ])),
                  Column(
                    children: <Widget>[
                      RadioListTile<CleanType>(
                        title: Text('current_session'.tr,
                            style: GoogleFonts.inter(
                              color: Colors.black,
                              fontSize: 13,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w400,
                            )),
                        value: CleanType.currentSession,
                        groupValue: _cleanType,
                        onChanged: (CleanType value) {
                          _cleanType = value;
                        },
                      ),
                      RadioListTile<CleanType>(
                        title: Text('one_day'.tr,
                            style: GoogleFonts.inter(
                              color: Colors.black,
                              fontSize: 13,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w400,
                            )),
                        value: CleanType.oneDay,
                        groupValue: _cleanType,
                        onChanged: (CleanType value) {
                          _cleanType = value;
                        },
                      ),
                      RadioListTile<CleanType>(
                        title: Text('one_week'.tr,
                            style: GoogleFonts.inter(
                              color: Colors.black,
                              fontSize: 13,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w400,
                            )),
                        value: CleanType.oneWeek,
                        groupValue: _cleanType,
                        onChanged: (CleanType value) {
                          _cleanType = value;
                        },
                      ),
                      RadioListTile<CleanType>(
                        title: Text('never'.tr,
                            style: GoogleFonts.inter(
                              color: Colors.black,
                              fontSize: 13,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w400,
                            )),
                        value: CleanType.never,
                        groupValue: _cleanType,
                        onChanged: (CleanType value) {
                          _cleanType = value;
                        },
                      )
                    ],
                  ),
                  ListTile(
                    isThreeLine: true,
                    title: Text('remove_all_trash'.tr,
                        style: GoogleFonts.inter(
                          color: Colors.black,
                          fontSize: 13,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w400,
                        )),
                    onTap: () {
                      Get.put(DrawerDisplayController()).updatedrawerType(1);
                    },
                    subtitle: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          TextButton(
                            child: Text('remove'.tr,
                                style: GoogleFonts.inter(
                                  color: Colors.white,
                                  fontSize: 17,
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w400,
                                )),
                            style: TextButton.styleFrom(
                              backgroundColor: Color(0xff3ACC3B),
                              primary: Colors.black,
                              minimumSize: Size(101, 33),
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20))),
                            ),
                            onPressed: () {
                              print('Pressed Remove');
                            },
                          )
                        ]),
                  ),
                  ListTile(
                    isThreeLine: true,
                    title: Text('select_duplicated_photos'.tr,
                        style: GoogleFonts.inter(
                          color: Colors.black,
                          fontSize: 13,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w400,
                        )),
                    onTap: () {
                      Get.put(DrawerDisplayController()).updatedrawerType(1);
                    },
                    subtitle: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          TextButton(
                            child: Text('select'.tr,
                                style: GoogleFonts.inter(
                                  color: Colors.white,
                                  fontSize: 17,
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w400,
                                )),
                            style: TextButton.styleFrom(
                              backgroundColor: Color(0xff3ACC3B),
                              primary: Colors.black,
                              minimumSize: Size(101, 33),
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20))),
                            ),
                            onPressed: () {
                              print('Pressed select_duplicated_photos');
                            },
                          )
                        ]),
                  ),
                  ListTile(
                    isThreeLine: true,
                    trailing:
                        Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
                      FlutterSwitch(
                        width: 60.0,
                        height: 32.0,
                        toggleSize: 32.0,
                        value: true,
                        borderRadius: 30.0,
                        padding: 0.0,
                        showOnOff: false,
                        inactiveColor: Colors.white,
                        activeColor: Colors.blue,
                        switchBorder: Border.all(
                          color: Colors.grey,
                          width: 0.5,
                        ),
                        toggleBorder: Border.all(
                          color: Colors.grey,
                          width: 0.5,
                        ),
                        onToggle: (val) {},
                      )
                    ]),
                    title: Padding(
                        padding: EdgeInsets.only(top: 20.0),
                        child: Text('show_tags'.tr,
                            style: GoogleFonts.inter(
                              color: Colors.black,
                              fontSize: 13,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w400,
                            ))),
                    onTap: () {},
                    subtitle: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Expanded(
                              child: Text('',
                                  overflow: TextOverflow.ellipsis,
                                  textAlign: TextAlign.left,
                                  maxLines: 3,
                                  style: GoogleFonts.inter(
                                    color: Colors.grey,
                                    fontSize: 12,
                                    fontStyle: FontStyle.italic,
                                    fontWeight: FontWeight.w400,
                                  )))
                        ]),
                  ),
                ])));
  }
}
