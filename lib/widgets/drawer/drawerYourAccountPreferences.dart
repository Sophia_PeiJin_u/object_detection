// import 'package:flutter/material.dart';
// import 'package:google_fonts/google_fonts.dart';
// import 'package:get/get.dart';
// import 'package:flutter_switch/flutter_switch.dart';
//
// class SnapplaDrawerYourAccountPreferences extends StatelessWidget {
//   SnapplaDrawerYourAccountPreferences({Key key, @required this.profile})
//       : super(key: key);
//   final Profile profile;
//
//   @override
//   Widget build(BuildContext context) {
//     bool status = true;
//     //print('RENDERING: ${{profileName}}');
//     return ListView(
//         shrinkWrap: true,
//         physics: ClampingScrollPhysics(),
//         children: <Widget>[
//           ListTile(
//             leading: Icon(Icons.location_on_outlined,
//                 color: Colors.black, size: 20.0),
//             trailing: SizedBox(
//                 width: 60,
//                 height: 32.0,
//                 child: FlutterSwitch(
//                   width: 60.0,
//                   height: 32.0,
//                   toggleSize: 32.0,
//                   value: Get.put(ProfileController())
//                       .findProfile(profile.name)
//                       .isLocation,
//                   borderRadius: 30.0,
//                   padding: 0.0,
//                   showOnOff: false,
//                   inactiveColor: Colors.white,
//                   activeColor: Colors.blue,
//                   switchBorder: Border.all(
//                     color: Colors.grey,
//                     width: 0.5,
//                   ),
//                   toggleBorder: Border.all(
//                     color: Colors.grey,
//                     width: 0.5,
//                   ),
//                   onToggle: (val) {
//                     //status = val;
//                     profile.isLocation = val;
//                     Get.put(ProfileController()).updateProfile(profile);
//                   },
//                 )),
//             title: Text('location'.tr,
//                 style: GoogleFonts.inter(
//                   color: Colors.black,
//                   fontSize: 13,
//                   fontStyle: FontStyle.normal,
//                   fontWeight: FontWeight.w400,
//                 )),
//             onTap: () {},
//           ),
//           ListTile(
//             leading: Icon(
//               Icons.access_time_outlined,
//               color: Colors.black,
//               size: 20.0,
//             ),
//             trailing: SizedBox(
//                 width: 60,
//                 height: 32.0,
//                 child: FlutterSwitch(
//                   width: 60.0,
//                   height: 32.0,
//                   toggleSize: 32.0,
//                   value: Get.put(ProfileController())
//                       .findProfile(profile.name)
//                       .isTime,
//                   borderRadius: 30.0,
//                   padding: 0.0,
//                   showOnOff: false,
//                   inactiveColor: Colors.white,
//                   activeColor: Colors.blue,
//                   switchBorder: Border.all(
//                     color: Colors.grey,
//                     width: 0.5,
//                   ),
//                   toggleBorder: Border.all(
//                     color: Colors.grey,
//                     width: 0.5,
//                   ),
//                   onToggle: (val) {
//                     profile.isTime = val;
//                     Get.put(ProfileController()).updateProfile(profile);
//                   },
//                 )),
//             title: Text('time'.tr,
//                 style: GoogleFonts.inter(
//                   color: Colors.black,
//                   fontSize: 13,
//                   fontStyle: FontStyle.normal,
//                   fontWeight: FontWeight.w400,
//                 )),
//             onTap: () {},
//           ),
//           ListTile(
//             leading: Icon(
//               Icons.attach_money_outlined,
//               color: Colors.black,
//               size: 20.0,
//             ),
//             trailing: SizedBox(
//                 width: 60,
//                 height: 32.0,
//                 child: FlutterSwitch(
//                   width: 60.0,
//                   height: 32.0,
//                   toggleSize: 32.0,
//                   value: Get.put(ProfileController())
//                       .findProfile(profile.name)
//                       .isPrice,
//                   borderRadius: 30.0,
//                   padding: 0.0,
//                   showOnOff: false,
//                   inactiveColor: Colors.white,
//                   activeColor: Colors.blue,
//                   switchBorder: Border.all(
//                     color: Colors.grey,
//                     width: 0.5,
//                   ),
//                   toggleBorder: Border.all(
//                     color: Colors.grey,
//                     width: 0.5,
//                   ),
//                   onToggle: (val) {
//                     profile.isPrice = val;
//                     Get.put(ProfileController()).updateProfile(profile);
//                   },
//                 )),
//             title: Text('price'.tr,
//                 style: GoogleFonts.inter(
//                   color: Colors.black,
//                   fontSize: 13,
//                   fontStyle: FontStyle.normal,
//                   fontWeight: FontWeight.w400,
//                 )),
//             onTap: () {},
//           ),
//           ListTile(
//             leading: Icon(
//               Icons.qr_code_outlined,
//               color: Colors.black,
//               size: 20.0,
//             ),
//             trailing: SizedBox(
//                 width: 60,
//                 height: 32.0,
//                 child: FlutterSwitch(
//                   width: 60.0,
//                   height: 32.0,
//                   toggleSize: 32.0,
//                   value: Get.put(ProfileController())
//                       .findProfile(profile.name)
//                       .isBarcode,
//                   borderRadius: 30.0,
//                   padding: 0.0,
//                   showOnOff: false,
//                   inactiveColor: Colors.white,
//                   activeColor: Colors.blue,
//                   switchBorder: Border.all(
//                     color: Colors.grey,
//                     width: 0.5,
//                   ),
//                   toggleBorder: Border.all(
//                     color: Colors.grey,
//                     width: 0.5,
//                   ),
//                   onToggle: (val) {
//                     profile.isBarcode = val;
//                     Get.put(ProfileController()).updateProfile(profile);
//                   },
//                 )),
//             title: Text('barcode'.tr,
//                 style: GoogleFonts.inter(
//                   color: Colors.black,
//                   fontSize: 13,
//                   fontStyle: FontStyle.normal,
//                   fontWeight: FontWeight.w400,
//                 )),
//             onTap: () {},
//           ),
//           ListTile(
//             leading: Icon(
//               Icons.picture_in_picture_outlined,
//               color: Colors.black,
//               size: 20.0,
//             ),
//             trailing: SizedBox(
//                 width: 60,
//                 height: 32.0,
//                 child: FlutterSwitch(
//                   width: 60.0,
//                   height: 32.0,
//                   toggleSize: 32.0,
//                   value: Get.put(ProfileController())
//                       .findProfile(profile.name)
//                       .isAdds,
//                   borderRadius: 30.0,
//                   padding: 0.0,
//                   showOnOff: false,
//                   inactiveColor: Colors.white,
//                   activeColor: Colors.blue,
//                   switchBorder: Border.all(
//                     color: Colors.grey,
//                     width: 0.5,
//                   ),
//                   toggleBorder: Border.all(
//                     color: Colors.grey,
//                     width: 0.5,
//                   ),
//                   onToggle: (val) {
//                     profile.isAdds = val;
//                     Get.put(ProfileController()).updateProfile(profile);
//                   },
//                 )),
//             title: Text('adds'.tr,
//                 style: GoogleFonts.inter(
//                   color: Colors.black,
//                   fontSize: 13,
//                   fontStyle: FontStyle.normal,
//                   fontWeight: FontWeight.w400,
//                 )),
//             onTap: () {},
//           ),
//           ListTile(
//             leading: Icon(
//               Icons.assessment_outlined,
//               color: Colors.black,
//               size: 20.0,
//             ),
//             trailing: SizedBox(
//                 width: 60,
//                 height: 32.0,
//                 child: FlutterSwitch(
//                   width: 60.0,
//                   height: 32.0,
//                   toggleSize: 32.0,
//                   value: Get.put(ProfileController())
//                       .findProfile(profile.name)
//                       .isSensors,
//                   borderRadius: 30.0,
//                   padding: 0.0,
//                   showOnOff: false,
//                   inactiveColor: Colors.white,
//                   activeColor: Colors.blue,
//                   switchBorder: Border.all(
//                     color: Colors.grey,
//                     width: 0.5,
//                   ),
//                   toggleBorder: Border.all(
//                     color: Colors.grey,
//                     width: 0.5,
//                   ),
//                   onToggle: (val) {
//                     profile.isSensors = val;
//                     Get.put(ProfileController()).updateProfile(profile);
//                   },
//                 )),
//             title: Text('sensors'.tr,
//                 style: GoogleFonts.inter(
//                   color: Colors.black,
//                   fontSize: 13,
//                   fontStyle: FontStyle.normal,
//                   fontWeight: FontWeight.w400,
//                 )),
//             onTap: () {},
//           ),
//           ListTile(
//             trailing: SizedBox(
//                 width: 60,
//                 height: 32.0,
//                 child: FlutterSwitch(
//                   width: 60.0,
//                   height: 32.0,
//                   toggleSize: 32.0,
//                   value: Get.put(ProfileController())
//                       .findProfile(profile.name)
//                       .isBarometr,
//                   borderRadius: 30.0,
//                   padding: 0.0,
//                   showOnOff: false,
//                   inactiveColor: Colors.white,
//                   activeColor: Colors.blue,
//                   switchBorder: Border.all(
//                     color: Colors.grey,
//                     width: 0.5,
//                   ),
//                   toggleBorder: Border.all(
//                     color: Colors.grey,
//                     width: 0.5,
//                   ),
//                   onToggle: (val) {
//                     profile.isBarometr = val;
//                     Get.put(ProfileController()).updateProfile(profile);
//                   },
//                 )),
//             title: Padding(
//                 padding: EdgeInsets.only(left: 55.0),
//                 child: Text('barometr'.tr,
//                     style: GoogleFonts.inter(
//                       color: Colors.black,
//                       fontSize: 13,
//                       fontStyle: FontStyle.normal,
//                       fontWeight: FontWeight.w400,
//                     ))),
//             onTap: () {},
//           ),
//           ListTile(
//             trailing: SizedBox(
//                 width: 60,
//                 height: 32.0,
//                 child: FlutterSwitch(
//                   width: 60.0,
//                   height: 32.0,
//                   toggleSize: 32.0,
//                   value: Get.put(ProfileController())
//                       .findProfile(profile.name)
//                       .isThreeAxisGyro,
//                   borderRadius: 30.0,
//                   padding: 0.0,
//                   showOnOff: false,
//                   inactiveColor: Colors.white,
//                   activeColor: Colors.blue,
//                   switchBorder: Border.all(
//                     color: Colors.grey,
//                     width: 0.5,
//                   ),
//                   toggleBorder: Border.all(
//                     color: Colors.grey,
//                     width: 0.5,
//                   ),
//                   onToggle: (val) {
//                     profile.isThreeAxisGyro = val;
//                     Get.put(ProfileController()).updateProfile(profile);
//                   },
//                 )),
//             title: Padding(
//                 padding: EdgeInsets.only(left: 55.0),
//                 child: Text('three_axis_gyro'.tr,
//                     style: GoogleFonts.inter(
//                       color: Colors.black,
//                       fontSize: 13,
//                       fontStyle: FontStyle.normal,
//                       fontWeight: FontWeight.w400,
//                     ))),
//             onTap: () {},
//           ),
//           ListTile(
//             trailing: SizedBox(
//                 width: 60,
//                 height: 32.0,
//                 child: FlutterSwitch(
//                   width: 60.0,
//                   height: 32.0,
//                   toggleSize: 32.0,
//                   value: Get.put(ProfileController())
//                       .findProfile(profile.name)
//                       .isAccelerometer,
//                   borderRadius: 30.0,
//                   padding: 0.0,
//                   showOnOff: false,
//                   inactiveColor: Colors.white,
//                   activeColor: Colors.blue,
//                   switchBorder: Border.all(
//                     color: Colors.grey,
//                     width: 0.5,
//                   ),
//                   toggleBorder: Border.all(
//                     color: Colors.grey,
//                     width: 0.5,
//                   ),
//                   onToggle: (val) {
//                     profile.isAccelerometer = val;
//                     Get.put(ProfileController()).updateProfile(profile);
//                   },
//                 )),
//             title: Padding(
//                 padding: EdgeInsets.only(left: 55.0),
//                 child: Text('accelerometer'.tr,
//                     style: GoogleFonts.inter(
//                       color: Colors.black,
//                       fontSize: 13,
//                       fontStyle: FontStyle.normal,
//                       fontWeight: FontWeight.w400,
//                     ))),
//             onTap: () {},
//           ),
//           ListTile(
//             trailing: SizedBox(
//                 width: 60,
//                 height: 32.0,
//                 child: FlutterSwitch(
//                   width: 60.0,
//                   height: 32.0,
//                   toggleSize: 32.0,
//                   value: Get.put(ProfileController())
//                       .findProfile(profile.name)
//                       .isProximity,
//                   borderRadius: 30.0,
//                   padding: 0.0,
//                   showOnOff: false,
//                   inactiveColor: Colors.white,
//                   activeColor: Colors.blue,
//                   switchBorder: Border.all(
//                     color: Colors.grey,
//                     width: 0.5,
//                   ),
//                   toggleBorder: Border.all(
//                     color: Colors.grey,
//                     width: 0.5,
//                   ),
//                   onToggle: (val) {
//                     profile.isProximity = val;
//                     Get.put(ProfileController()).updateProfile(profile);
//                   },
//                 )),
//             title: Padding(
//                 padding: EdgeInsets.only(left: 55.0),
//                 child: Text('proximity'.tr,
//                     style: GoogleFonts.inter(
//                       color: Colors.black,
//                       fontSize: 13,
//                       fontStyle: FontStyle.normal,
//                       fontWeight: FontWeight.w400,
//                     ))),
//             onTap: () {},
//           ),
//           ListTile(
//             trailing: SizedBox(
//                 width: 60,
//                 height: 32.0,
//                 child: FlutterSwitch(
//                   width: 60.0,
//                   height: 32.0,
//                   toggleSize: 32.0,
//                   value: Get.put(ProfileController())
//                       .findProfile(profile.name)
//                       .isAmbientLight,
//                   borderRadius: 30.0,
//                   padding: 0.0,
//                   showOnOff: false,
//                   inactiveColor: Colors.white,
//                   activeColor: Colors.blue,
//                   switchBorder: Border.all(
//                     color: Colors.grey,
//                     width: 0.5,
//                   ),
//                   toggleBorder: Border.all(
//                     color: Colors.grey,
//                     width: 0.5,
//                   ),
//                   onToggle: (val) {
//                     profile.isAmbientLight = val;
//                     Get.put(ProfileController()).updateProfile(profile);
//                   },
//                 )),
//             title: Padding(
//                 padding: EdgeInsets.only(left: 55.0),
//                 child: Text('ambient_light'.tr,
//                     style: GoogleFonts.inter(
//                       color: Colors.black,
//                       fontSize: 13,
//                       fontStyle: FontStyle.normal,
//                       fontWeight: FontWeight.w400,
//                     ))),
//             onTap: () {},
//           ),
//           ListTile(
//             trailing: SizedBox(
//                 width: 60,
//                 height: 32.0,
//                 child: FlutterSwitch(
//                     width: 60.0,
//                     height: 32.0,
//                     toggleSize: 0.0,
//                     value: status,
//                     borderRadius: 30.0,
//                     padding: 0.0,
//                     showOnOff: false,
//                     inactiveColor: Colors.white,
//                     activeColor: Get.put(ProfileController())
//                         .findProfile(profile.name)
//                         .colour,
//                     switchBorder: Border.all(
//                       color: Colors.grey,
//                       width: 0.5,
//                     ),
//                     toggleBorder: Border.all(
//                       color: Colors.grey,
//                       width: 0.5,
//                     ),
//                     onToggle: (val) {
//                       status = val;
//                       print('color tap');
//                       showDialog(
//                           context: context,
//                           builder: (BuildContext context) {
//                             return AlertDialog(
//                               //title: Text('Select a color'),
//                               content: SingleChildScrollView(
//                                 child: ColorPicker(
//                                   color: Get.put(ProfileController())
//                                       .findProfile(profile.name)
//                                       .colour,
//                                   showMaterialName: false,
//                                   showColorName: false,
//                                   showColorCode: false,
//                                   pickersEnabled: const <ColorPickerType, bool>{
//                                     ColorPickerType.both: false,
//                                     ColorPickerType.primary: false,
//                                     ColorPickerType.accent: false,
//                                     ColorPickerType.bw: false,
//                                     ColorPickerType.custom: false,
//                                     ColorPickerType.wheel: false,
//                                   },
//                                   onColorChanged: (Color color) {
//                                     profile.colour = color;
//                                     Get.put(ProfileController())
//                                         .updateProfile(profile);
//                                   },
//                                   width: 44,
//                                   height: 44,
//                                   borderRadius: 2,
//                                   heading: Text(
//                                     'select_colour'.tr,
//                                     style: GoogleFonts.inter(
//                                       color: Colors.black,
//                                       fontSize: 16,
//                                       fontStyle: FontStyle.normal,
//                                       fontWeight: FontWeight.w600,
//                                     ),
//                                   ),
//                                 ),
//                               ),
//                             );
//                           });
//                     })),
//             title: Padding(
//                 padding: EdgeInsets.only(left: 35.0),
//                 child: Text('colour'.tr,
//                     style: GoogleFonts.inter(
//                       color: Colors.black,
//                       fontSize: 13,
//                       fontStyle: FontStyle.normal,
//                       fontWeight: FontWeight.w400,
//                     ))),
//             onTap: () {},
//           ),
//         ]);
//   }
// }
