import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:get/get.dart';
import 'package:object_detection/controller/drawerDisplayController.dart';

class SnapplaDrawerMain extends StatelessWidget {
  const SnapplaDrawerMain({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: Container(
          color: Color(0xFF5F5F5),
          child: new ListView(
            // Important: Remove any padding from the ListView.
            padding: EdgeInsets.zero,
            children: <Widget>[
              Container(
                height: 100.0,
                child: DrawerHeader(
                  child: Text('snappla_settings'.tr,
                      textAlign: TextAlign.center,
                      style: GoogleFonts.inter(
                        color: Color(0xff1874E5),
                        fontSize: 17,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w400,
                      )),
                ),
              ),
              ListTile(
                leading: Icon(Icons.qr_code, color: Colors.black),
                trailing: Icon(Icons.chevron_right_outlined),
                title: Text('share_your_account'.tr,
                    style: GoogleFonts.inter(
                      color: Colors.black,
                      fontSize: 12,
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.w400,
                    )),
                onTap: () {
                  // Get.to(SnapplaShareYourAccount());
                },
              ),
              ListTile(
                leading: Icon(Icons.account_circle_outlined, color: Colors.black),
                trailing: Icon(Icons.chevron_right_outlined),
                title: Text('your_account'.tr,
                    style: GoogleFonts.inter(
                      color: Colors.black,
                      fontSize: 12,
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.w400,
                    )),
                onTap: () {
                  Get.put(DrawerDisplayController()).updatedrawerType(2);
                },
              ),
              ListTile(
                leading: Icon(Icons.photo_camera_outlined, color: Colors.black),
                trailing: Icon(Icons.chevron_right_outlined),
                title: Text('camera_mode'.tr,
                    style: GoogleFonts.inter(
                      color: Colors.black,
                      fontSize: 12,
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.w400,
                    )),
                onTap: () {
                  Get.put(DrawerDisplayController()).updatedrawerType(3);
                },
              ),
              ListTile(
                leading: Icon(Icons.delete_outlined, color: Colors.black),
                trailing: Icon(Icons.chevron_right_outlined),
                title: Text('snappla_trash'.tr,
                    style: GoogleFonts.inter(
                      color: Colors.black,
                      fontSize: 12,
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.w400,
                    )),
                onTap: () {
                  Get.put(DrawerDisplayController()).updatedrawerType(4);
                },
              ),
              ListTile(
                leading: Icon(Icons.add_circle_outline, color: Colors.black),
                trailing: Icon(Icons.chevron_right_outlined),
                title: Text('add_new_product'.tr,
                    style: GoogleFonts.inter(
                      color: Colors.black,
                      fontSize: 12,
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.w400,
                    )),
                onTap: () {
                  Get.put(DrawerDisplayController()).updatedrawerType(5);
                },
              ),
              ListTile(
                leading: Icon(Icons.filter_list_outlined, color: Colors.black),
                trailing: Icon(Icons.chevron_right_outlined),
                title: Text('product_list'.tr,
                    style: GoogleFonts.inter(
                      color: Colors.black,
                      fontSize: 12,
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.w400,
                    )),
                onTap: () {
                  Get.put(DrawerDisplayController()).updatedrawerType(6);
                },
              ),
              ListTile(
                leading: Icon(Icons.contact_mail_outlined, color: Colors.black),
                trailing: Icon(Icons.chevron_right_outlined),
                title: Text('product_card'.tr,
                    style: GoogleFonts.inter(
                      color: Colors.black,
                      fontSize: 12,
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.w400,
                    )),
                onTap: () {
                  Get.put(DrawerDisplayController()).updatedrawerType(7);
                },
              ),
              ListTile(
                leading: Icon(Icons.group_outlined, color: Colors.black),
                trailing: Icon(Icons.chevron_right_outlined),
                title: Text('profiles'.tr,
                    style: GoogleFonts.inter(
                      color: Colors.black,
                      fontSize: 12,
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.w400,
                    )),
                onTap: () {
                  // Get.to(SnapplaProfilesList());
                },
              ),
            ],
          ),
        ));
  }
}
