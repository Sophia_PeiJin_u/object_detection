import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:get/get.dart';
import 'package:object_detection/controller/drawerDisplayController.dart';

enum LocationSelection { iosMap, googleMap }
enum FilterSelection { filterOn, filterOff }

class SnapplaDrawerProductList extends StatelessWidget {
  const SnapplaDrawerProductList({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    LocationSelection _locationSelection = LocationSelection.iosMap;
    FilterSelection _filterSelection = FilterSelection.filterOn;
    return Drawer(
        child: Container(
            color: Color(0xFF5F5F5),
            child: ListView(
                shrinkWrap: true,
                physics: ClampingScrollPhysics(),
                padding: const EdgeInsets.all(20.0),
                children: <Widget>[
                  ListTile(
                    leading: Icon(Icons.chevron_left_outlined),
                    title: Text('product_list'.tr,
                        style: GoogleFonts.inter(
                          color: Colors.black,
                          fontSize: 17,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w400,
                        )),
                    onTap: () {
                      Get.put(DrawerDisplayController()).updatedrawerType(1);
                    },
                  ),
                  ListTile(
                      //isThreeLine: true,
                      onTap: () {},
                      trailing: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            ToggleButtons(
                              renderBorder: false,
                              //selectedColor: Colors.blue,
                              //selectedBorderColor: Colors.grey,
                              constraints:
                                  BoxConstraints.expand(width: 30, height: 30),
                              children: <Widget>[
                                Icon(Icons.grid_view, size: 18),
                                Icon(Icons.list, size: 18),
                                Icon(Icons.computer, size: 18),
                              ],
                              onPressed: (int index) {},
                              isSelected: [true, false, false],
                            ),
                          ]),
                      title: Text('display'.tr,
                          style: GoogleFonts.inter(
                            color: Colors.black,
                            fontSize: 12,
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.w400,
                          ))),
                  Padding(
                      padding: EdgeInsets.only(left: 16),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Text('location'.tr,
                                style: GoogleFonts.inter(
                                  color: Colors.black,
                                  fontSize: 12,
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w400,
                                ))
                          ])),
                  Padding(
                      padding: EdgeInsets.only(left: 16),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                                child: Text('location_dsc'.tr,
                                    overflow: TextOverflow.ellipsis,
                                    //textDirection: TextDirection.rtl,
                                    textAlign: TextAlign.left,
                                    maxLines: 3,
                                    style: GoogleFonts.inter(
                                      color: Colors.grey,
                                      fontSize: 12,
                                      fontStyle: FontStyle.italic,
                                      fontWeight: FontWeight.w400,
                                    )))
                          ])),
                  Column(
                    children: <Widget>[
                      RadioListTile<LocationSelection>(
                        title: Text('ios_map'.tr,
                            style: GoogleFonts.inter(
                              color: Colors.black,
                              fontSize: 13,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w400,
                            )),
                        value: LocationSelection.iosMap,
                        groupValue: _locationSelection,
                        onChanged: (LocationSelection value) {
                          _locationSelection = value;
                        },
                      ),
                      RadioListTile<LocationSelection>(
                        title: Text('google_map'.tr,
                            style: GoogleFonts.inter(
                              color: Colors.black,
                              fontSize: 13,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w400,
                            )),
                        value: LocationSelection.googleMap,
                        groupValue: _locationSelection,
                        onChanged: (LocationSelection value) {
                          _locationSelection = value;
                        },
                      ),
                    ],
                  ),
                  Padding(
                      padding: EdgeInsets.only(left: 16),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Text('turn_filters'.tr,
                                style: GoogleFonts.inter(
                                  color: Colors.black,
                                  fontSize: 12,
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w400,
                                ))
                          ])),
                  Padding(
                      padding: EdgeInsets.only(left: 16),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                                child: Text('turn_filters_dsc'.tr,
                                    overflow: TextOverflow.ellipsis,
                                    //textDirection: TextDirection.rtl,
                                    textAlign: TextAlign.left,
                                    maxLines: 3,
                                    style: GoogleFonts.inter(
                                      color: Colors.grey,
                                      fontSize: 12,
                                      fontStyle: FontStyle.italic,
                                      fontWeight: FontWeight.w400,
                                    )))
                          ])),
                  Column(
                    children: <Widget>[
                      RadioListTile<FilterSelection>(
                        title: Text('turn_on_all_details'.tr,
                            style: GoogleFonts.inter(
                              color: Colors.black,
                              fontSize: 13,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w400,
                            )),
                        value: FilterSelection.filterOn,
                        groupValue: _filterSelection,
                        onChanged: (FilterSelection value) {
                          _filterSelection = value;
                        },
                      ),
                      RadioListTile<FilterSelection>(
                        title: Text('turn_off_all_details'.tr,
                            style: GoogleFonts.inter(
                              color: Colors.black,
                              fontSize: 13,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w400,
                            )),
                        value: FilterSelection.filterOff,
                        groupValue: _filterSelection,
                        onChanged: (FilterSelection value) {
                          _filterSelection = value;
                        },
                      ),
                    ],
                  ),
                  ListTile(
                    isThreeLine: true,
                    title: Text('remember_filter_settings'.tr,
                        style: GoogleFonts.inter(
                          color: Colors.black,
                          fontSize: 13,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w400,
                        )),
                    onTap: () {},
                    subtitle: Column(children: <Widget>[
                      Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                                child: Text('remember_filter_settings_dsc'.tr,
                                    overflow: TextOverflow.ellipsis,
                                    textAlign: TextAlign.left,
                                    maxLines: 3,
                                    style: GoogleFonts.inter(
                                      color: Colors.grey,
                                      fontSize: 12,
                                      fontStyle: FontStyle.italic,
                                      fontWeight: FontWeight.w400,
                                    )))
                          ]),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            TextButton(
                              child: Text('save'.tr,
                                  style: GoogleFonts.inter(
                                    color: Colors.white,
                                    fontSize: 17,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.w400,
                                  )),
                              style: TextButton.styleFrom(
                                backgroundColor: Color(0xff3ACC3B),
                                primary: Colors.black,
                                minimumSize: Size(101, 33),
                                shape: RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(20))),
                              ),
                              onPressed: () {
                                print('Pressed Save');
                              },
                            )
                          ])
                    ]),
                  ),
                ])));
  }
}
