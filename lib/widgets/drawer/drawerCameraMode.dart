import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:get/get.dart';
import 'package:object_detection/controller/drawerDisplayController.dart';

import 'package:flutter_switch/flutter_switch.dart';

class SnapplaDrawerCameraMode extends StatelessWidget {
  const SnapplaDrawerCameraMode({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: Container(
            color: Color(0xFF5F5F5),
            child: ListView(
                shrinkWrap: true,
                physics: ClampingScrollPhysics(),
                padding: const EdgeInsets.all(20.0),
                children: <Widget>[
                  ListTile(
                    leading: Icon(Icons.chevron_left_outlined),
                    title: Text('camera_mode'.tr,
                        style: GoogleFonts.inter(
                          color: Colors.black,
                          fontSize: 17,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w400,
                        )),
                    onTap: () {
                      Get.put(DrawerDisplayController()).updatedrawerType(1);
                    },
                  ),
                  ListTile(
                    trailing: SizedBox(
                        width: 60,
                        height: 32.0,
                        child: FlutterSwitch(
                          width: 60.0,
                          height: 32.0,
                          toggleSize: 32.0,
                          value: true,
                          borderRadius: 30.0,
                          padding: 0.0,
                          showOnOff: false,
                          inactiveColor: Colors.white,
                          activeColor: Colors.blue,
                          switchBorder: Border.all(
                            color: Colors.grey,
                            width: 0.5,
                          ),
                          toggleBorder: Border.all(
                            color: Colors.grey,
                            width: 0.5,
                          ),
                          onToggle: (val) {},
                        )),
                    title: Padding(
                        padding: EdgeInsets.only(left: 0.0),
                        child: Text('reduce_results'.tr,
                            style: GoogleFonts.inter(
                              color: Colors.black,
                              fontSize: 13,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w400,
                            ))),
                    onTap: () {},
                  ),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                            child: Padding(
                                padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                                child: Text('reduce_results_dsc'.tr,
                                    overflow: TextOverflow.ellipsis,
                                    textAlign: TextAlign.left,
                                    maxLines: 3,
                                    style: GoogleFonts.inter(
                                      color: Colors.grey,
                                      fontSize: 12,
                                      fontStyle: FontStyle.italic,
                                      fontWeight: FontWeight.w400,
                                    ))))
                      ]),
                  ListTile(
                    trailing: SizedBox(
                        width: 60,
                        height: 32.0,
                        child: FlutterSwitch(
                          width: 60.0,
                          height: 32.0,
                          toggleSize: 32.0,
                          value: true,
                          borderRadius: 30.0,
                          padding: 0.0,
                          showOnOff: false,
                          inactiveColor: Colors.white,
                          activeColor: Colors.blue,
                          switchBorder: Border.all(
                            color: Colors.grey,
                            width: 0.5,
                          ),
                          toggleBorder: Border.all(
                            color: Colors.grey,
                            width: 0.5,
                          ),
                          onToggle: (val) {},
                        )),
                    title: Padding(
                        padding: EdgeInsets.only(left: 0.0),
                        child: Text('geo_location'.tr,
                            style: GoogleFonts.inter(
                              color: Colors.black,
                              fontSize: 13,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w400,
                            ))),
                    onTap: () {},
                  ),
                ])));
  }
}
