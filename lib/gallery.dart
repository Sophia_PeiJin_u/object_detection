import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:object_detection/controller/productController.dart';

class Gallery extends StatefulWidget {
  @override
  _GalleryState createState() => _GalleryState();
}

class _GalleryState extends State<Gallery> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        leading: TextButton(
          child: Icon(
            Icons.chevron_left,
            size: 20.0,
            color: Colors.black,
          ),
          style: TextButton.styleFrom(
            backgroundColor: Colors.transparent,
            primary: Colors.black,
            minimumSize: Size(30, 30),
            shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
          ),
          onPressed: () {
            Get.back();
          },
        ),
        backgroundColor: Colors.white,
        title: Text('Added photos'.tr,
            style: GoogleFonts.inter(
              color: Colors.black,
              fontSize: 17,
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.w400,
            )),
        centerTitle: true,
        actions: <Widget>[
          TextButton(
            child: Icon(
              Icons.settings_outlined,
              size: 20.0,
              color: Colors.black,
            ),
            style: TextButton.styleFrom(
              backgroundColor: Colors.transparent,
              primary: Colors.black,
              minimumSize: Size(30, 30),
              shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
            ),
            onPressed: () {
              Get.back();
              //Get.put(DrawerDisplayController()).updatedrawerType(1);
              //Scaffold.of(context).openEndDrawer();
            },
          ),
        ],
      ),
      body: Container(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Container(
                  child: Text(
                      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
                  style: GoogleFonts.inter(
                    color: Colors.grey,
                    fontSize: 13,
                    fontStyle: FontStyle.italic,
                    fontWeight: FontWeight.w400,
                  ),
                  ),
              ),
            ),
            Container(
              color: Color(0xffF5F5F5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  TextButton(onPressed: (){
                    ProductController.to.initProduct();
                        setState(() {});
                  }, child: Row(
                    children: [
                      Icon(Icons.clear),
                      Text('Delete'),
                    ],
                  ),
                  ),
                  TextButton(onPressed: (){}, child: Row(
                    children: [
                      Icon(Icons.done_all),
                      Text('Select all'),
                    ],
                  ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Obx(() => GridView.builder(
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                  ),
                  itemCount: ProductController.to.product.value.photos.length,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        height: 500,
                        child: Column(
                          children: [
                            Image.file(
                              File(ProductController
                                  .to.product.value.photos[index]),
                              height: 80,
                              width: 80,
                              fit: BoxFit.cover,
                            ),
                            Expanded(
                              child: ListView.builder(
                                  itemCount: ProductController
                                      .to.product.value.results[index].length,
                                  itemBuilder: (context, index2) {
                                    return Text(
                                        'Label: ${ProductController.to.product.value.results[index][index2].label}, score: ${ProductController.to.product.value.results[index][index2].score}');
                                  }),
                            ),
                          ],
                        ),
                      ),
                    );
                  })),
            ),
          ],
        ),
      ),
      // floatingActionButton: FloatingActionButton(
      //   onPressed: () {
      //     print('upload images');
      //     ProductController.to.initProduct();
      //     setState(() {});
      //   },
      //   child: const Icon(Icons.navigation),
      // ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Get.back();
        },
        child: const Icon(Icons.camera_alt_outlined),
      ),
    );
  }
}
