import 'dart:io';
import 'dart:isolate';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tags/flutter_tags.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:object_detection/controller/productController.dart';
import 'package:object_detection/preview.dart';
import 'package:object_detection/tflite/classifier.dart';
import 'package:object_detection/tflite/recognition.dart';
import 'package:object_detection/tflite/stats.dart';
import 'package:object_detection/ui/camera_view_singleton.dart';
import 'package:object_detection/gallery.dart';
import 'package:object_detection/utils/isolate_utils.dart';
import 'package:path_provider/path_provider.dart';

/// [CameraView] sends each frame for inference
class CameraView extends StatefulWidget {
  /// Callback to pass results after inference to [HomeView]
  final Function(List<Recognition> recognitions) resultsCallback;

  /// Callback to inference stats to [HomeView]
  final Function(Stats stats) statsCallback;

  /// Constructor
  const CameraView(this.resultsCallback, this.statsCallback);
  @override
  _CameraViewState createState() => _CameraViewState();
}

class _CameraViewState extends State<CameraView> with WidgetsBindingObserver {
  /// List of available cameras
  List<CameraDescription> cameras;

  /// Controller
  CameraController cameraController;

  /// true when inference is ongoing
  bool predicting;

  /// Instance of [Classifier]
  Classifier classifier;

  /// Instance of [IsolateUtils]
  IsolateUtils isolateUtils;

  final photoResult = GetStorage();

  Map<String, dynamic> inferenceResults;

  ProductController productController = Get.put(ProductController());

  onCapture(context) async {
    cameraController.stopImageStream();
    try {
      final p = await getTemporaryDirectory();
      final name = DateTime.now();
      final path = "${p.path}/$name.png";

      await cameraController.takePicture(path).then((value) {
        print('here');
        print(path);
        photoResult.write('imgPath', path);
        photoResult.write('inferenceResults', inferenceResults['recognitions']);
        ProductController.to.addNewProduct(
            photoResult.read('imgPath').toString(),
            photoResult.read('inferenceResults'));
        // Get.to(() => PreviewScreen(imgPath: path,fileName: "$name.png", inferenceResults: inferenceResults,));
        // Navigator.push(context, MaterialPageRoute(builder: (context) =>PreviewScreen(imgPath: path,fileName: "$name.png", inferenceResults: inferenceResults,)));
      });
    } catch (e) {
      print(e);
    }
    cameraController.startImageStream(onLatestImageAvailable);
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);

    // Camera initialization
    initializeCamera();

    // Create an instance of classifier to load model and labels
    classifier = Classifier();

    // Initially predicting = false
    predicting = false;

    // Spawn a new isolate
    isolateUtils = IsolateUtils();
    isolateUtils.start();
  }

  /// Initializes the camera by setting [cameraController]
  void initializeCamera() async {
    cameras = await availableCameras();

    // cameras[0] for rear-camera
    cameraController =
        CameraController(cameras[0], ResolutionPreset.low, enableAudio: false);

    cameraController.initialize().then((_) async {
      // Stream of image passed to [onLatestImageAvailable] callback
      await cameraController.startImageStream(onLatestImageAvailable);

      /// previewSize is size of each image frame captured by controller
      ///
      /// 352x288 on iOS, 240p (320x240) on Android with ResolutionPreset.low
      Size previewSize = cameraController.value.previewSize;

      /// previewSize is size of raw input image to the model
      CameraViewSingleton.inputImageSize = previewSize;

      // the display width of image on screen is
      // same as screenWidth while maintaining the aspectRatio
      Size screenSize = MediaQuery.of(context).size;
      CameraViewSingleton.screenSize = screenSize;
      CameraViewSingleton.ratio = screenSize.width / previewSize.height;
    });
  }

  @override
  Widget build(BuildContext context) {
    // Return empty container while the camera is not initialized
    if (cameraController == null || !cameraController.value.isInitialized) {
      return Container();
    }

    return Column(
      children: [
        AspectRatio(
          aspectRatio: cameraController.value.aspectRatio,
          child: Stack(
            children: [
              CameraPreview(cameraController),
              Align(
                alignment: Alignment.bottomCenter,
                child: Obx(
                  //Obx is very similar to GetX except 'lighter' so no parameters for init, dispose, etc
                  () => Tags(
                    horizontalScroll: true,
                    itemCount: ProductController.to.product.value.tags.length, // required
                    itemBuilder: (int index) {
                      final item =
                          ProductController.to.product.value.tags[index];
                      return ItemTags(
                        // Each ItemTags must contain a Key. Keys allow Flutter to
                        // uniquely identify widgets.
                        //padding: EdgeInsets.only(top: 20),
                        key: Key(index.toString()),
                        index: index, // required
                        title: item,
                        textStyle: GoogleFonts.inter(
                          color: Colors.white,
                          fontSize: 12,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w400,
                        ),
                        textColor: Colors.white,
                        borderRadius: BorderRadius.circular(60),
                        elevation: 0,
                        combine: ItemTagsCombine.onlyText,
                        color: Color(0xff1874E5),
                        activeColor: Color(0xff4F4F4F),
                        // onPressed: (item) => print(item),
                        // onLongPressed: (item) => print(item),
                      );
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
        Table(
          children: [
            TableRow(children: [
              TextButton(
                onPressed: () {
                  Get.to(() => Gallery());
                },
                child: ProductController.to.product.value.photos.isEmpty
                    ? Container(
                        height: 80,
                        width: 50,
                        child: Text(''),
                      )
                    : Image.file(
                        File(ProductController.to.product.value.photos.last),
                        height: 50,
                        width: 50,
                        fit: BoxFit.cover,
                      ),
              ),
              FlatButton(
                height: 100,
                color: Colors.transparent,
                onPressed: () {
                  onCapture(context);
                },
                child: Container(
                  width: 75,
                  height: 75,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle, color: Colors.white),
                ),
                shape: CircleBorder(
                  side: BorderSide(color: Colors.white, width: 2),
                ),
              ),
              FlatButton(
                onPressed: () => {print("Snaplla private")},
                color: Colors.transparent,
                padding: EdgeInsets.all(1.0),
                child: Column(
                  // Replace with a Row for horizontal icon + text
                  children: <Widget>[
                    Icon(
                      Icons.people_outline,
                      color: Colors.white,
                      size: 28.0,
                    ),
                    Text('private'.tr,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 17,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w400,
                        ))
                  ],
                ),
              )
            ]),
          ],
        ),
      ],
    );
  }

  /// Callback to receive each frame [CameraImage] perform inference on it
  onLatestImageAvailable(CameraImage cameraImage) async {
    if (classifier.interpreter != null && classifier.labels != null) {
      // If previous inference has not completed then return
      if (predicting) {
        return;
      }

      Future.delayed(const Duration(milliseconds: 5000), () {
        ProductController.to.resetTags();
      });

      setState(() {
        predicting = true;
      });

      var uiThreadTimeStart = DateTime.now().millisecondsSinceEpoch;

      // Data to be passed to inference isolate
      var isolateData = IsolateData(
          cameraImage, classifier.interpreter.address, classifier.labels);

      // We could have simply used the compute method as well however
      // it would be as in-efficient as we need to continuously passing data
      // to another isolate.

      /// perform inference in separate isolate
      inferenceResults = await inference(isolateData);

      var uiThreadInferenceElapsedTime =
          DateTime.now().millisecondsSinceEpoch - uiThreadTimeStart;

      // pass results to HomeView
      widget.resultsCallback(inferenceResults["recognitions"]);

      for (Recognition recognition in inferenceResults["recognitions"]) {
        //print("LABEL:${recognition.label} Score: ${recognition.score}");
        ProductController.to.updateTags(recognition.label);
      }

      // pass stats to HomeView
      widget.statsCallback((inferenceResults["stats"] as Stats)
        ..totalElapsedTime = uiThreadInferenceElapsedTime);

      // set predicting to false to allow new frames
      setState(() {
        predicting = false;
      });
    }
  }

  /// Runs inference in another isolate
  Future<Map<String, dynamic>> inference(IsolateData isolateData) async {
    ReceivePort responsePort = ReceivePort();
    isolateUtils.sendPort
        .send(isolateData..responsePort = responsePort.sendPort);
    var results = await responsePort.first;
    return results;
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    switch (state) {
      case AppLifecycleState.paused:
        cameraController.stopImageStream();
        break;
      case AppLifecycleState.resumed:
        await cameraController.startImageStream(onLatestImageAvailable);
        break;
      default:
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    cameraController.dispose();
    super.dispose();
  }
}
