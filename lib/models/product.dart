import 'package:object_detection/tflite/recognition.dart';

class Product {
  List<List<Recognition>> results;
  List<String> photos;
  List<String> tags;

  Product({this.results, this.photos, this.tags});

}