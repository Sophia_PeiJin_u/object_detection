import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:object_detection/controller/productController.dart';
import 'package:object_detection/gallery.dart';

// class PreviewScreen extends StatefulWidget {
//   final String imgPath;
//   final String fileName;
//   final Map<String, dynamic> inferenceResults;
//   PreviewScreen({this.imgPath, this.fileName, this.inferenceResults});
//
//   @override
//   _PreviewScreenState createState() => _PreviewScreenState();
// }

class PreviewScreen extends StatelessWidget {
  final String imgPath;
  final String fileName;
  final Map<String, dynamic> inferenceResults;
  PreviewScreen({this.imgPath, this.fileName, this.inferenceResults});


  final photoResult = GetStorage();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: true,
        ),
        body: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Image.file(
                  File(imgPath),
                  fit: BoxFit.cover,
                ),
              ),
              Expanded(
                flex: 1,
                child: Container(
                  width: double.infinity,
                  height: 60,
                  color: Colors.white,
                  child: Column(
                    children: [
                      Center(
                        // child: IconButton(
                        //   icon: Icon(Icons.share,color: Colors.white,),
                        //   onPressed: (){
                        //     getBytes().then((bytes) {
                        //       print('here now');
                        //       print(widget.imgPath);
                        //       print(bytes.buffer.asUint8List());
                        //     });
                        //   },
                        // ),
                        child: Text('${inferenceResults}'),
                      ),
                      Row(
                        children: [
                          TextButton(
                              onPressed: () {
                                photoResult.write('imgPath', imgPath);
                                photoResult.write('inferenceResults', inferenceResults['recognitions']);
                                ProductController.to.addNewProduct(photoResult.read('imgPath').toString(), photoResult.read('inferenceResults'));
                                Get.dialog(
                                  AlertDialog(content: Text(photoResult.read('imgPath').toString() + photoResult.read('inferenceResults').toString()),),
                                );
                              },
                              child: Text('Save')),
                          TextButton(
                              onPressed: () {
                                Get.to(() => Gallery());
                              },
                              child: Text('Upload')),
                        ],
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ));
  }

  Future getBytes() async {
    Uint8List bytes = File(imgPath).readAsBytesSync() as Uint8List;
//    print(ByteData.view(buffer))
    return ByteData.view(bytes.buffer);
  }
}
