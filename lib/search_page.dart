import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:object_detection/ui/home_view.dart';

class SearchPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        leading: TextButton(
          child: Icon(
            Icons.chevron_left,
            size: 20.0,
            color: Colors.black,
          ),
          style: TextButton.styleFrom(
            backgroundColor: Colors.transparent,
            primary: Colors.black,
            minimumSize: Size(30, 30),
            shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
          ),
          onPressed: () {
            Get.back();
          },
        ),
        backgroundColor: Colors.white,
        title: Text('Search'.tr,
            style: GoogleFonts.inter(
              color: Colors.black,
              fontSize: 17,
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.w400,
            )),
        centerTitle: true,
        actions: <Widget>[
          TextButton(
            child: Icon(
              Icons.settings_outlined,
              size: 20.0,
              color: Colors.black,
            ),
            style: TextButton.styleFrom(
              backgroundColor: Colors.transparent,
              primary: Colors.black,
              minimumSize: Size(30, 30),
              shape:
              CircleBorder(side: BorderSide(color: Colors.transparent)),
            ),
            onPressed: () {
              Get.back();
              //Get.put(DrawerDisplayController()).updatedrawerType(1);
              //Scaffold.of(context).openEndDrawer();
            },
          ),
        ],
        bottom: PreferredSize(
          child: Container(
            padding: EdgeInsets.fromLTRB(18.0, 0.0, 18.0, 10.0),
            //padding: EdgeInsets.only(bottom: 20),
            //width: 340.0,
            //height: 40.0,
            child: TextField(
              autofocus: false,
              style: GoogleFonts.inter(
                color: Colors.black,
                fontSize: 14,
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.w400,
              ),
              decoration: InputDecoration(
                fillColor: Color(0xffF5F5F5),
                focusColor: Color(0xffF5F5F5),
                hoverColor: Color(0xffF5F5F5),
                filled: true,
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.transparent),
                  borderRadius: BorderRadius.circular(30),
                ),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.transparent),
                  borderRadius: BorderRadius.circular(30),
                ),
                suffixIcon: TextButton(
                  child: Icon(
                    Icons.mic,
                    size: 20.0,
                    color: Colors.black,
                  ),
                  style: TextButton.styleFrom(
                    backgroundColor: Colors.transparent,
                    primary: Colors.black,
                    minimumSize: Size(30, 30),
                    shape: CircleBorder(
                        side: BorderSide(color: Colors.transparent)),
                  ),
                  onPressed: () {
                    print('speach to text runnung');
                  },
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(30),
                  ),
                ),
                hintText: 'search for...'.tr,
              ),
            ),
          ),
          preferredSize: Size(340.0, 40.0),
        ),
      ),
      body: Container(
        color: Color(0xffF5F5F5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            TextButton(onPressed: (){}, child: Row(
              children: [
                Icon(Icons.timer),
                Text('Recent searches'),
              ],
            ),
            ),
            Row(
              children: [
                TextButton(onPressed: (){}, child: Row(
                  children: [
                    Icon(Icons.swap_vert),
                    Text('Sort by'),
                  ],
                ),
                ),
                TextButton(onPressed: (){}, child: Row(
                  children: [
                    Icon(Icons.sort),
                    Text('Filter'),
                  ],
                ),
                ),
              ],
            ),
          ],
        ),
      ),
      bottomNavigationBar: Container(
          decoration: BoxDecoration(
            //color: Colors.black,
            border: Border.all(color: Colors.grey, width: 0.3),
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(30), topLeft: Radius.circular(30)),
          ),
          padding: const EdgeInsets.all(10.0),
          //color: Colors.transparent,
          child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                TextButton(
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Icon(Icons.search),
                      ]),
                  style: TextButton.styleFrom(
                    backgroundColor: Colors.transparent,
                    primary: Colors.black,
                    minimumSize: Size(40, 40),
                    shape: RoundedRectangleBorder(
                        side: BorderSide(
                            color: Colors.transparent,
                            width: 1,
                            style: BorderStyle.solid),
                        borderRadius: BorderRadius.all(Radius.circular(20))),
                  ),
                  onPressed: () {
                    print('bottom nav search');
                  },
                ),
                TextButton(
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Icon(Icons.group_outlined),
                      ]),
                  style: TextButton.styleFrom(
                    backgroundColor: Colors.transparent,
                    primary: Colors.black,
                    minimumSize: Size(40, 40),
                    shape: RoundedRectangleBorder(
                        side: BorderSide(
                            color: Colors.transparent,
                            width: 1,
                            style: BorderStyle.solid),
                        borderRadius: BorderRadius.all(Radius.circular(20))),
                  ),
                  onPressed: () {
                    print('bottom nav group');
                  },
                ),
                TextButton(
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Icon(Icons.camera_alt_outlined),
                      ]),
                  style: TextButton.styleFrom(
                    backgroundColor: Colors.transparent,
                    primary: Colors.black,
                    minimumSize: Size(40, 40),
                    shape: RoundedRectangleBorder(
                        side: BorderSide(
                            color: Colors.transparent,
                            width: 1,
                            style: BorderStyle.solid),
                        borderRadius: BorderRadius.all(Radius.circular(20))),
                  ),
                  onPressed: () {
                    Get.to(() => HomeView());
                  },
                )
              ])),
    );
  }
}
