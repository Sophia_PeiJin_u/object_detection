import 'package:get/get.dart';
import 'package:object_detection/widgets/drawer/drawerCameraMode.dart';
import 'package:object_detection/widgets/drawer/drawerNewProduct.dart';
import 'package:object_detection/widgets/drawer/drawerProductCard.dart';
import 'package:object_detection/widgets/drawer/drawerProductList.dart';
import 'package:object_detection/widgets/drawer/drawerTrash.dart';
import 'package:object_detection/widgets/drawer/drawerYourAccount.dart';
import 'package:object_detection/widgets/drawer/drawer_main.dart';


class DrawerDisplayController extends GetxController {

  static DrawerDisplayController get to => Get.find();

  var drawerType = 1.obs;
  var predictRun = false.obs;
  //set _drawerType(value) => this.drawerType.value = value;
  //get _drawerType => this.drawerType.value;

  updatedrawerType(typeId) {
    drawerType.value = typeId;
  }

  updatepredictRun(bool isRun) {
    predictRun.value = isRun;
  }

  getDrawerClass() {
    if (drawerType.value == 1) {
      // no actual function
      return SnapplaDrawerMain();
    } else if (drawerType.value == 2) {
      // no actual function
      return SnapplaDrawerMain();
      // return SnapplaDrawerYourAccount();
    } else if (drawerType.value == 3) {
      return SnapplaDrawerCameraMode();
    } else if (drawerType.value == 4) {
      return SnapplaDrawerTrash();
    } else if (drawerType.value == 5) {
      return SnapplaDrawerNewProduct();
    } else if (drawerType.value == 6) {
      return SnapplaDrawerProductList();
    } else if (drawerType.value == 7) {
      return SnapplaDrawerProductCard();
    }
  }
}
