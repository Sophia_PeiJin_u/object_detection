import 'package:get/get.dart';
import 'package:object_detection/models/product.dart';
import 'package:object_detection/tflite/recognition.dart';

class ProductController extends GetxController {
  final product = Product().obs;
  static ProductController get to => Get.find();

  @override
  void onInit() {
    ever(product, (_) => print("$_ has been changed (ProductController ever)"));
    initProduct();
    super.onInit();
  }

  initProduct() {
    product.value.results = [];
    product.value.photos = [];
    product.value.tags = [];
  }

  resetTags() {
    product.value.tags = [];
  }

  addNewProduct(String photo, List<Recognition> tag) {
    //print("ADD TAG --> ${tag}");
    product.value.results.add(tag);

    product.value.photos.add(photo);
  }

  updateTags(String tag) {
    if (!product.value.tags.contains(tag)) {
      product.value.tags.add(tag);
    }
  }
}
