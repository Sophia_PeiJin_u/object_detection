import 'dart:io';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:object_detection/controller/productController.dart';

class ImageController extends GetxController {
  static ImageController get to => Get.find();

  File image;
  String imagePath;
  final _picker = ImagePicker();

  Future<void> getImage() async {
    final pickedFile = await _picker.getImage(source: ImageSource.gallery);

    if (pickedFile != null) {
      image = File(pickedFile.path);
      imagePath = pickedFile.path;
      print(imagePath);
      // photoResult.write('imgPath', imagePath);
      // photoResult.write('inferenceResults', inferenceResults['recognitions']);
      // ProductController.to.addNewProduct(
      //     photoResult.read('imgPath').toString(),
      //     photoResult.read('inferenceResults'));
    } else {
      print('No image selected.');
    }
  }
}